<?php
/**
 * @file
 * Contains \UpdateApiLog.
 */

/**
 * Enhanced terminal logging for update hooks.
 */
class UpdateApiLog {
  // Status indicators for logging.
  const INFO = 2;
  const GOOD = 1;
  const BAD = 0;

  /**
   * Status labels.
   *
   * @var array
   */
  private static $statusLabel = array(
    self::BAD => 'error',
    self::GOOD => 'status',
    self::INFO => 'warning',
  );

  /**
   * Log a message through the proper method during the update process.
   *
   * @param string $msg
   *   The message to log.
   * @param int $status
   *   The status constant for the message.
   */
  public static function log($msg, $status = self::GOOD) {
    if (drupal_is_cli()) {
      $display_status = static::getIcon($status);
      print "{$display_status} {$msg}\n";
    }
    else {
      $display_status = self::$statusLabel[$status];
      drupal_set_message($msg, $display_status);
    }
  }

  /**
   * Get a color-coded icon.
   *
   * @param int $status
   *   A log message status constant.
   *
   * @return string
   *   The colorized icon.
   */
  private static function getIcon($status) {
    switch ($status) {
      case self::BAD:
        return static::formatRed('✗');

      case self::GOOD:
        return static::formatGreen('✓');

      case self::INFO:
      default:
        return static::formatYellow('⚠︎︎');
    }
  }

  /**
   * Make a string green.
   *
   * @param string $string
   *   A string.
   *
   * @return string
   *   The string formatted to appear green.
   */
  public static function formatGreen($string) {
    $green = "\033[1;32;40m\033[1m%s\033[0m";
    $colored = sprintf($green, $string);

    return $colored;
  }

  /**
   * Make a string red.
   *
   * @param string $string
   *   A string.
   *
   * @return string
   *   The string formatted to appear red.
   */
  public static function formatRed($string) {
    $red = "\033[31;40m\033[1m%s\033[0m";
    $colored = sprintf($red, $string);

    return $colored;
  }

  /**
   * Make a string yello.
   *
   * @param string $string
   *   A string.
   *
   * @return string
   *   The string formatted to appear yellow.
   */
  public static function formatYellow($string) {
    $yellow = "\033[1;33;40m\033[1m%s\033[0m";
    $colored = sprintf($yellow, $string);

    return $colored;
  }

}
