<?php
/**
 * @file
 * Contains \UpdateApiBatch.
 */

require_once __DIR__ . '/UpdateApi.php';

/**
 * Facilitate simpler batch processing in update hooks.
 *
 * Extend this class in your .install file ensuring the class name is globally
 * unique.  Implement the two required methods: setIds() and processItem().
 *
 * @code
 * class UpdateApiBatchYourModule7101 extends UpdateApiBatch {
 *
 *   protected $perRun = 25;
 *
 *   protected function setIds() {
 *     // Get array of $ids.
 *
 *     return $ids;
 *   }
 *
 *   protected function processItem($id) {
 *     // Process the item by its $id.
 *   }
 *
 * }
 * @endcode
 */
abstract class UpdateApiBatch {

  const HOOK_BATCH_TIMER = 'update_api_hook_batch_helper';

  /**
   * The sandbox array from the update hook.
   *
   * @var array
   */
  private $sandbox;

  /**
   * A human readable title of the batch.
   *
   * @var string
   */
  private $title;

  /**
   * Flag to determine whether the batch process has been initialized yet.
   *
   * @var bool
   */
  private $initialized = FALSE;

  /**
   * The total number of items to process.
   *
   * @var int
   */
  private $totalCount = 0;

  /**
   * The number of items that have been processed.
   *
   * @var int
   */
  private $processed = 0;

  /**
   * The original ids to process.
   *
   * @var array
   */
  private $ids = array();

  /**
   * The number of items to process on each run.
   *
   * May be extended.
   *
   * @var int
   */
  protected $perRun = 100;

  /**
   * Singleton loader to fetch the existing batch process.
   *
   * @param array $sandbox
   *   The sandbox array from the update hook.
   * @param string $key
   *   A unique identifier for a batch process.  Usually __FUNCTION__ is used.
   * @param string $title
   *   An optional title to print when starting a batch process.
   *
   * @return static
   *   This batch object.
   */
  public static function load(array &$sandbox, $key, $title = '') {
    static $instance;

    if (!isset($instance[$key])) {
      $instance[$key] = new static($sandbox, $title);
    }

    return $instance[$key];
  }

  /**
   * BatchUpdateHelper constructor.
   *
   * @param array $sandbox
   *   The sandbox array from the update hook.
   * @param string $title
   *   An optional title to print when starting a batch process.
   */
  public function __construct(array &$sandbox, $title = '') {
    $this->sandbox = &$sandbox;
    $this->title = $title;
  }

  /**
   * Process a batch.
   */
  public function process() {
    // Initialize the batch on the first run, otherwise process a batch.
    if (!$this->initialized) {
      $this->initialize();
    }
    else {
      $this->run();
    }
  }

  /**
   * Conditional wrapper to setup a batch process.
   *
   * @return bool
   *   TRUE if the process needs to be initialized.
   */
  private function initialize() {
    if ($this->initialized) {
      return FALSE;
    }
    else {
      UpdateApi::logInfo(t("Starting Batch Process: @title", array('@title' => $this->title)));
      timer_start(self::HOOK_BATCH_TIMER);

      $this->ids = (array) $this->setIds();
      $this->totalCount = count($this->ids);

      $this->sandbox['#finished'] = FALSE;

      $this->initialized = TRUE;
      return TRUE;
    }
  }

  /**
   * Internal function to process the batch.
   */
  private function run() {
    // Process the batch.
    for ($i = 0; !empty($this->ids) && $i < $this->perRun; $i++) {
      $value = array_shift($this->ids);

      $this->processItem($value);

      $this->processed++;
    }

    // Log the process.
    $args = array(
      '!done' => $this->processed,
      '!total' => $this->totalCount,
      '!pct' => @round(($this->processed / $this->totalCount) * 100, 2),
    );
    UpdateApi::logSuccess(t('Completed !done/!total (!pct%)', $args));

    // Mark the process as completed or not.
    $this->sandbox['#finished'] = $this->isFinished();

    // If finished, print the timer.
    if ($this->isFinished()) {
      $timer = timer_stop(self::HOOK_BATCH_TIMER);
      UpdateApi::logSuccess(t("Batch completed in !time.", array('!time' => format_interval($timer['time'] / 1000, 3))));
    }
  }

  /**
   * Determine if the batch is finished.
   *
   * @return bool
   *   TRUE if all items have been processed.
   */
  public function isFinished() {
    return empty($this->ids);
  }

  /**
   * Get the IDs to process.
   *
   * @return array
   *   An array of IDs to pass into UpdateApiBatch::run().
   */
  abstract protected function setIds();

  /**
   * Process a single item from a batch.
   *
   * @param mixed $value
   *   A single batch value to process.
   */
  abstract protected function processItem($value);

}
