<?php

/**
 * @file
 * Contains \UpdateApi.
 */

require_once __DIR__ . '/UpdateApiLog.php';

/**
 * Simplify repetitive install tasks.
 */
class UpdateApi {

  /**
   * Disable one or more modules.
   *
   * @param mixed $modules
   *   A single module name or array of module names.
   *
   * @throws UpdateApiException
   *   If a module was not disabled.
   */
  public static function disableModules($modules) {
    $modules = (array) $modules;
    module_disable($modules, FALSE);

    foreach ($modules as $module) {
      if (!static::verifyDisabled($module)) {
        throw new UpdateApiException(t("Failed to disable !mod.", array('!mod' => $module)));
      }
    }

    static::logSuccess("Disabled modules: " . implode(', ', $modules));
  }

  /**
   * Uninstall one or more modules.
   *
   * @param mixed $modules
   *   A single module name or array of module names.
   *
   * @throws UpdateApiException
   *   If a module was not uninstalled.
   */
  public static function uninstallModules($modules) {
    $modules = (array) $modules;
    $args = array('!mods' => implode(', ', $modules));

    // Disable modules first.
    static::disableModules($modules);

    // Uninstall and verify.
    if (drupal_uninstall_modules($modules, FALSE)) {
      // Verify by checking the system table.
      foreach ($modules as $module) {
        if (!static::verifyUninstalled($module)) {
          throw new UpdateApiException(t("Failed to uninstall !mod.  Update failed.", array('!mod' => $module)));
        }
      }
    }
    else {
      throw new UpdateApiException(t("Failed to uninstall modules !mods. Update failed.", $args));
    }

    static::logSuccess(t("Uninstalled modules: !mods", $args));
  }

  /**
   * Enable one or more modules.
   *
   * @param mixed $modules
   *   A single module name or array of module names.
   *
   * @throws \UpdateApiException
   *   If the modules could not be enabled.
   */
  public static function enableModules($modules) {
    $modules = (array) $modules;
    $args = array('!mods' => implode(', ', $modules));

    if (!module_enable($modules)) {
      throw new UpdateApiException(t("Failed to enable !mods.", $args));
    }

    static::logSuccess(t("Enabled modules: !mods", $args));
  }

  /**
   * Delete a field, it's instances, data and DB tables.
   *
   * @param mixed $field_names
   *   A single field name or an array of field names.
   *
   * @throws UpdateApiException
   *   If the field fails to delete.
   *
   * @todo: Wrap field helper conditional.
   * @todo: t() wrap strings.
   */
  public static function deleteFields($field_names) {
    foreach ((array) $field_names as $field_name) {
      if ($field = field_info_field($field_name)) {
        try {
          FieldHelper::deleteField($field, TRUE);
          static::logSuccess("Deleted field {$field_name}.");
        }
        catch (Exception $e) {
          throw new UpdateApiException($e->getMessage());
        }
      }
      else {
        static::logInfo("Unable to load field {$field_name} for deleting.");
      }
    }
  }

  /**
   * Delete a field instance.
   *
   * @param string $entity_type
   *   The type of the entity the instance is attached to.
   * @param string $bundle
   *   The bundle the instance is attached to.
   * @param string $field_name
   *   The machine name of this field.
   *
   * @throws UpdateApiException
   *   If the field instance fails to delete.
   *
   * @todo: Wrap field helper conditional.
   * @todo: t() wrap strings.
   */
  public static function deleteFieldInstance($entity_type, $bundle, $field_name) {
    $instance = field_info_instance($entity_type, $field_name, $bundle);
    if ($instance) {
      try {
        FieldHelper::deleteInstance($instance);
        static::logSuccess("Deleted field instance {$field_name} on {$entity_type} / {$bundle}.");
      }
      catch (Exception $e) {
        throw new UpdateApiException($e->getMessage());
      }
    }
    else {
      static::logInfo("Unable to load field instance {$field_name} on {$entity_type}:{$bundle} for deleting.");
    }
  }

  /**
   * Revert an entire feature module or just certain components.
   *
   * If $module is an array of modules and $components is not empty, then those
   * components will be reverted for all $modules listed.  To revert different
   * components in each module, call the method once per module.
   *
   * @param string|array $module
   *   Module(s) to revert.
   * @param string|array $components
   *   Optional component(s) of the module(s) to revert.
   *
   * @todo: Wrap features module conditional.
   * @todo: t() wrap strings.
   */
  public static function revertFeatures($module, $components = NULL) {
    foreach ((array) $module as $_module) {
      if (empty($components)) {
        features_revert_module($_module);
        static::logSuccess("Reverted {$_module}.");
      }
      else {
        $components = (array) $components;
        features_revert(array($_module => $components));
        static::logSuccess("Reverted " . implode(', ', $components) . " in {$_module}.");
      }

    }
  }

  /**
   * Lock a whole feature or some of its components.
   *
   * @param string $module
   *   A feature module machine name.
   * @param string|array $components
   *   (optional) One or more components to lock.
   *
   * @todo: Wrap features module conditional.
   */
  public static function lockFeature($module, $components = NULL) {
    if ($components) {
      foreach ((array) $components as $component) {
        features_feature_lock($module, $component);
        static::logSuccess(t("Locked component @comp of feature @module.", array('@component' => $component, '@module' => $module)));
      }
    }
    else {
      features_feature_lock($module);
      static::logSuccess(t("Locked feature @module.", array('@module' => $module)));
    }
  }

  /**
   * Set the timing rule for a cron job.
   *
   * @param string $job_name
   *   The full cron job name.
   * @param string $rule
   *   The cron timing rule.
   */
  public static function setCronJobRule($job_name, $rule) {
    static::setCronRule($job_name, $rule);
  }

  /**
   * Set the timing rule for a cron queue.
   *
   * @param string $job_name
   *   The cron queue name, not including the prefix 'queue_'.
   * @param string $rule
   *   The cron timing rule.
   */
  public static function setCronQueueRule($job_name, $rule) {
    static::setCronRule('queue_' . $job_name, $rule);
  }

  /**
   * Helper function to set the elysia settings for a cron task.
   *
   * @param string $job_name
   *   The full cron job name.
   * @param string $rule
   *   The cron timing rule.
   *
   * @todo: Write a missing module exception.
   */
  private static function setCronRule($job_name, $rule) {
    if (!module_exists('elysia_cron')) {
      static::logError(t("Module elysia_cron is required to run setCronRule."));
      return;
    }

    elysia_cron_set($job_name, FALSE, array('rule' => $rule));
    static::logSuccess("Set cron task {$job_name} to {$rule}.");
  }

  /**
   * Set a variable value.
   *
   * @param string $name
   *   The var name.
   * @param mixed $value
   *   The var value.
   *
   * @todo: t() wrap strings.
   */
  public static function setVariable($name, $value) {
    variable_set($name, $value);
    static::logSuccess("Set variable {$name} to {$value}.");
  }

  /**
   * Delete variables.
   *
   * @param string|array $names
   *   The var name or array of names.
   *
   * @todo: t() wrap strings.
   */
  public static function deleteVariables($names) {
    foreach ((array) $names as $name) {
      variable_del($name);
      static::logSuccess("Deleted variable {$name}.");
    }
  }

  /**
   * Disable a view.
   *
   * @param string $view_name
   *   The machine name of the view to disable.
   *
   * @todo: t() wrap strings.
   */
  public static function disableView($view_name) {
    $views = variable_get('views_defaults', array());
    $views[$view_name] = TRUE;
    variable_set('views_defaults', $views);
    static::logSuccess("Disabled view {$view_name}");
  }

  /**
   * Helper function to create a redirect.
   *
   * @param string $source
   *   The source URL.
   * @param string $destination
   *   The destination URL.
   * @param array $source_options
   *   (optional) The source options, defaults to an empty array.
   * @param array $destination_options
   *   (optional) The destination options, defaults to an empty array.
   * @param string $langcode
   *   (optional) The redirect language code. Defaults to LANGUAGE_NONE.
   *
   * @see redirect_object_prepare()
   *
   * @todo: Write a missing module exception.
   */
  public static function createRedirect($source, $destination, array $source_options = array(), array $destination_options = array(), $langcode = LANGUAGE_NONE) {
    if (!module_exists('redirect')) {
      return;
    }

    $redirect = new stdClass();
    redirect_object_prepare(
      $redirect,
      array(
        'source' => $source,
        'source_options' => $source_options,
        'redirect' => $destination,
        'redirect_options' => $destination_options,
        'language' => $langcode,
      )
    );
    redirect_save($redirect);
    self::logSuccess(t('Created redirect from !source to !redirect', array('!source' => $source, '!redirect' => $destination)));
  }

  /**
   * Delete a flag from the system.
   *
   * @param string|array $flag_names
   *   The names of the flags to delete.
   *
   * @throws \UpdateApiException
   *   If the flag is not found.
   *
   * @todo: Write a missing module exception.
   */
  public static function deleteFlags($flag_names) {
    if (!module_exists('flag')) {
      return;
    }

    foreach ((array) $flag_names as $flag_name) {
      if ($flag = flag_get_flag($flag_name)) {
        $flag->delete();
        $flag->disable();
        _flag_clear_cache();
        static::logSuccess("Deleted flag $flag_name.");
      }
      else {
        throw new UpdateApiException(t("Unable to locate flag @flag for deletion.", array('@flag' => $flag_name)));
      }
    }
  }

  /**
   * Remove records from the system table for dead modules.
   *
   * @param string|array $modules
   *   The name(s) of modules to wipe from the system table.
   */
  public static function systemWipe($modules) {
    db_delete('system')->condition('name', $modules)->execute();

    $names = implode(', ', (array) $modules);
    static::logSuccess("Wiped {$names} from system table.");
  }

  /**
   * Manually drop a database table.
   *
   * @param string|array $tables
   *   One or more table names.
   *
   * @todo: t() wrap strings.
   */
  public static function dropTables($tables) {
    foreach ((array) $tables as $table) {
      // Existence check happens in the DB object prior to drop.
      if (db_drop_table($table)) {
        static::logSuccess("Dropped table {$table}.");
      }
      else {
        static::logInfo("Table {$table} was not dropped because it does not exist.");
      }
    }
  }

  /**
   * Place a block in a theme's region.
   *
   * @param string $module
   *   The module that owns the block.
   * @param string $delta
   *   The block delta.
   * @param string $theme
   *   The theme in which to place the block.
   * @param string $region
   *   The region in the theme.
   * @param array $settings
   *   Additional settings, such as cache value or weight.
   *
   * @throws \UpdateApiException
   */
  public static function placeBlock($module, $delta, $theme, $region, array $settings = array()) {
    // Define the required fields to set and necessary default values.
    // SQL cols that don't have default values must be set here in  case the
    // merge results in an insert.
    $fields = array(
      'region' => $region,
      'status' => 1,
      'pages' => '',
    );

    // ESI adds cols to the block table.
    if (db_field_exists('block', 'esi_ttl')) {
      $fields['esi_ttl'] = 0;
    }

    // Add any additional settings to the query.
    foreach ($settings as $col => $val) {
      $fields[$col] = $val;
    }

    try {
      // Upsert the record.
      $res = db_merge('block')
        ->key(array(
          'theme' => $theme,
          'module' => $module,
          'delta' => $delta,
        ))
        ->fields($fields)
        ->execute();
    }
    catch (Exception $e) {
      // Cutoff-exception to relay through our custom handler.
      throw new UpdateApiException($e->getMessage(), $e->getCode());
    }

    // Define and set the message.
    $t_args = array(
      '@block' => $delta,
      '@theme' => $theme,
      '@region' => $region,
    );

    // If the block record was successfully updated, it should return that at
    // least one row was affected.
    if ($res > 0) {
      static::logSuccess(t("Block @block was placed in the @theme @region.", $t_args));
    }
    else {
      throw new UpdateApiException(t("Failed to place block @block in the @theme @region.", $t_args));
    }
  }

  /**
   * Create a new taxonomy term.
   *
   * @param array|string $term
   *   An array of basic term data or just a name.
   * @param object|string $vocabulary
   *   A vocabulary object or just a vocabulary name.
   *
   * @return object|void
   *   The taxonomy term or NULL if the process fails.
   *
   * @todo: t() wrap strings.
   */
  public static function createTerm($term, $vocabulary) {
    if (is_string($vocabulary)) {
      $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary);
    }
    elseif (!is_object($vocabulary)) {
      static::logError("Invalid vocabulary name.");
      return;
    }

    // Allow terms to be passed in as string (term names) or arrays containing
    // more term data, such as weight.
    if (is_array($term)) {
      $term = (object) $term;
    }
    else {
      $_term = new stdClass();
      $_term->name = $term;
      $term = $_term;
    }

    // Set the vocabulary.
    $term->vid = $vocabulary->vid;

    $t_args = array(
      '@name' => $term->name,
      '@vocab' => $vocabulary->name,
    );

    switch (taxonomy_term_save($term)) {
      case SAVED_NEW:
        static::logSuccess(t("Added term '@name' to '@vocab'.", $t_args));
        break;

      case SAVED_UPDATED:
        static::logInfo(t("Updated term '@name' in '@vocab'.", $t_args));
        break;

      default:
        static::logError(t("Unable to create term '@name' in '@vocab'.", $t_args));
        break;
    }

    return $term;
  }

  /**
   * Add new terms to a taxonomy vocabulary.
   *
   * @param string|array $terms
   *   A single term name or array of term names.
   * @param string $vocabulary_name
   *   The machine name of the vocabulary to add to.
   *
   * @return array
   *   An array of the created terms.
   */
  public static function createTerms($terms, $vocabulary_name) {
    $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name);

    foreach ((array) $terms as &$term) {
      $term = static::createTerm($term, $vocabulary);
    }

    return $terms;
  }

  /**
   * Delete a taxonomy term.
   *
   * @param int $tid
   *   A valid term id.
   *
   * @throws \UpdateApiException
   *   If there was a problem deleting the term.
   */
  public static function deleteTerm($tid) {
    try {
      if (taxonomy_term_delete($tid) === SAVED_DELETED) {
        static::logSuccess(t('Deleted term !tid', array('!tid' => $tid)));
      }
      else {
        static::logError(t('Error deleting term !tid', array('!tid' => $tid)));
      }
    }
    catch (Exception $e) {
      // Cutoff-exception to relay through our custom handler.
      throw new UpdateApiException($e->getMessage(), $e->getCode());
    }
  }

  /**
   * Delete a taxonomy vocabulary and all its terms.
   *
   * @param int|string $id
   *   A vocab VID or machine name.
   *
   * @throws \UpdateApiException
   *   If the vocab is found but not deleted.
   */
  public static function deleteVocabulary($id) {
    try {
      if (is_numeric($id) && ($vocab = taxonomy_vocabulary_load($id))) {
        taxonomy_vocabulary_delete($id);
        static::logSuccess(t("Deleted vocabulary !name and all its terms.", array('!name' => $vocab->name)));
      }
      elseif (is_string($id) && ($vocab = taxonomy_vocabulary_machine_name_load($id))) {
        taxonomy_vocabulary_delete($vocab->vid);
        static::logSuccess(t("Deleted vocabulary !name and all its terms.", array('!name' => $vocab->name)));
      }
      else {
        static::logInfo(t("Unable to locate a taxonomy vocabulary matching VID or machine name !id.", array('!id' => $id)));
      }
    }
    catch (Exception $e) {
      // Cutoff-exception to relay through our custom handler.
      throw new UpdateApiException($e->getMessage(), $e->getCode());
    }
  }

  /**
   * Delete a context record.
   *
   * @param string $name
   *   The machine name of the context.
   *
   * @throws \UpdateApiException
   *   If the context is found but not deleted.
   */
  public static function deleteContext($name) {
    if (!module_exists('context')) {
      return;
    }

    $args = array('!name' => $name);

    if (($context = context_load($name)) && ($context->export_type & EXPORT_IN_DATABASE)) {
      if (context_delete($context)) {
        static::logSuccess(t("Deleted context !name.", $args));
      }
      else {
        throw new UpdateApiException(t("Failed to delete context !name.", array($args)));
      }
    }
    else {
      static::logInfo(t("Unable to locate a context by the name !name for deletion.", $args));
    }
  }

  /**
   * Delete a view.
   *
   * @param string $name
   *   The machine name of a view.
   *
   * @throws \UpdateApiException
   *   If the view is found but not able to be deleted.
   */
  public static function deleteView($name) {
    if (!module_exists('views')) {
      return;
    }

    $args = array('!name' => $name);

    if (($view = views_get_view($name)) && !empty($view->vid)) {
      if (views_delete_view($view)) {
        static::logSuccess(t("Deleted view !name.", $args));
      }
      else {
        throw new UpdateApiException(t("Failed to delete view !name.", $args));
      }
    }
    else {
      static::logInfo(t("Unable to locate a view by the name !name for deletion.", $args));
    }
  }

  /**
   * Delete a node type.
   *
   * All nodes of this type must be deleted prior to running this command.
   *
   * @param string $name
   *   A node type machine name.
   *
   * @throws \UpdateApiException
   *   If there are nodes of this type still in the system.
   */
  public static function deleteNodeType($name) {
    // Ensure there are no nodes of this type still around.
    $count = db_select('node')
      ->fields('node')
      ->condition('node.type', $name)
      ->execute()
      ->rowCount();

    $args = array('!name' => $name, '!cnt' => $count);

    if ($count == 0) {
      node_type_delete($name);
      static::logSuccess(t("Deleted node type !name", $args));
    }
    else {
      throw new UpdateApiException(t("Unable to delete node type !name. There are !cnt nodes of this type in the system.", $args));
    }
  }

  /**
   * Delete variables from a strongarm file.
   *
   * @param string $module
   *   The machine name for a module containing a strongarm file.
   *
   * @throws \UpdateApiException
   *   If the strongarm file cannot be loaded.
   */
  public static function deleteStrongarmVariables($module) {
    $function = "{$module}_strongarm";
    module_load_include('inc', $module, $module . '.strongarm');

    if (function_exists($function)) {
      $variables = $function();
      static::deleteVariables(array_keys($variables));
    }
    else {
      throw new UpdateApiException(t("Unable to load strongarm file for !name module.", array('!name' => $module)));
    }
  }

  /**
   * Toggle the reporting of apachesolr's ReadOnly warnings.
   *
   * Requires patch from https://www.drupal.org/node/2863134.
   *
   * @param bool $toggle
   *   TRUE to enable logging, FALSE to suppress it.
   *
   * @todo: t() wrap strings.
   */
  public static function apachesolrReadOnlyWarnings($toggle) {
    variable_set('apachesolr_report_readonly_to_watchdog', $toggle);

    if ($toggle === FALSE) {
      static::logInfo("Suppressing apachesolr readonly warnings.");
    }
    else {
      static::logInfo("Enabling apachesolr readonly warnings.");
    }
  }

  /**
   * Log a success message.
   *
   * @param string $msg
   *   An translated string.
   */
  public static function logSuccess($msg) {
    UpdateApiLog::log($msg, UpdateApiLog::GOOD);
  }

  /**
   * Log an info message.
   *
   * @param string $msg
   *   An translated string.
   */
  public static function logInfo($msg) {
    UpdateApiLog::log($msg, UpdateApiLog::INFO);
  }

  /**
   * Log a failure message.
   *
   * This is used over an exception if the failure should not kill the update
   * process.
   *
   * @param string $msg
   *   An translated string.
   */
  public static function logError($msg) {
    UpdateApiLog::log($msg, UpdateApiLog::BAD);
  }


  /**
   * Verify if a module or theme is uninstalled.
   *
   * @param string $module_name
   *   A module or theme name.
   *
   * @return bool|null
   *   TRUE if the module or theme is uninstalled, FALSE otherwise.  NULL if the
   *   module wasn't found.
   */
  protected static function verifyUninstalled($module_name) {
    $sql = "SELECT status, schema_version FROM {system} WHERE name = :name";
    $args = array(':name' => $module_name);

    // $res will be FALSE if there are no results.
    if ($res = db_query($sql, $args)->fetchAssoc()) {
      $status = (int) $res['status'];
      $schema_version = (int) $res['schema_version'];

      return (($status === DRUPAL_DISABLED) && ($schema_version === -1));
    }
  }

  /**
   * Verify that a module was successfully disabled.
   *
   * @param string $module_name
   *   A module or theme name.
   *
   * @return bool|null
   *   TRUE if disabled, FALSE otherwise.  NULL if the module was not found.
   */
  protected static function verifyDisabled($module_name) {
    $sql = "SELECT status FROM {system} WHERE name = :name";
    $args = array(':name' => $module_name);

    $status = db_query($sql, $args)->fetchField();
    if (is_numeric($status)) {
      $status = (int) $status;

      return ($status === DRUPAL_DISABLED);
    }
  }

}

/**
 * Handle logging of exceptions to the terminal.
 */
class UpdateApiException extends DrupalUpdateException {

  /**
   * {@inheritdoc}
   */
  public function __construct($message = "", $code = UpdateApiLog::BAD, \Exception $previous = NULL) {
    // Log the custom error message ourselves.
    UpdateApiLog::log($message, UpdateApiLog::BAD);

    // Override the exception message as we already logged the custom message.
    $function = $this->getUpdateFunction();
    $message = UpdateApiLog::formatRed("Update $function failed!");

    parent::__construct($message, $code, $previous);
  }

  /**
   * Get the function name of the update hook.
   *
   * @return string|null
   *   The function name if found.  NULL otherwise.
   */
  private function getUpdateFunction() {
    foreach ($this->getTrace() as $item) {
      if (preg_match('/_\d{4}$/', $item['function'])) {
        return $item['function'];
      }
    }
  }

}
